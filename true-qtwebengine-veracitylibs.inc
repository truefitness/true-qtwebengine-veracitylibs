export FILES=source
export PACKAGE_NAME=true-qtwebengine-veracitylibs
# These files are installed by postins, so placing them in tmp is safe
export VERSION=1.0
export MAINTAINER=Mike Nolan
export MAINTAINER_EMAIL=mpnolan@truefitness.com
export SIZE=$(shell du $(FILES) | awk '{print $$1}')
export BUILD_PATH=$(PACKAGE_NAME)_$(VERSION)
PKG_DIR=./queue

# Define the Control File
define CONTROLDATA
Package: $(PACKAGE_NAME)
Version: $(VERSION)
Architecture: i386
Maintainer: $(MAINTAINER) <$(MAINTAINER_EMAIL)>
Installed-Size: $(SIZE)
Section: main 
Priority: extra
Homepage: http://www.truefitness.com
Description: 
 - 1.0 Initial release
 .
 * Add libstdc++6.so from libstdc++6 (= 5.2.1-22ubuntu2)
 * Add libc6 libraries from libc6 (= 6_2.19-0ubuntu6.7)
 * Add libxkbcommon0, libxkbcommon-x11-0. These are required
 due to their dependency on the newer libc6 also included here.

endef

export CONTROLDATA

define PREINST
endef
export PREINST

define POSTRM
endef
export POSTRM

define POSTINST
endef
export POSTINST

true-qtwebengine-veracitylibs:
	-@mkdir -p $(BUILD_PATH)/DEBIAN
	echo "$$CONTROLDATA" > $(BUILD_PATH)/DEBIAN/control
	echo "$$PREINST" > $(BUILD_PATH)/DEBIAN/preinst
	chmod 0775 $(BUILD_PATH)/DEBIAN/preinst
	echo "$$POSTINST" > $(BUILD_PATH)/DEBIAN/postinst
	chmod 0775 $(BUILD_PATH)/DEBIAN/postinst
	echo "$$POSTRM" > $(BUILD_PATH)/DEBIAN/postrm
	chmod 0775 $(BUILD_PATH)/DEBIAN/postrm
	cp -r $(FILES)/* $(BUILD_PATH)
	dpkg --build $(BUILD_PATH)
	-@mkdir -p $(PKG_DIR)
	mv *.deb $(PKG_DIR)
clean:
	-rm -rf $(PACKAGE_NAME)_*
	-rm -rf $(PKG_DIR)

